import { AppComponent } from './app.component';
import { NoteComponent } from './notes/note/note.component';
import { SharednotesComponent } from './notes/sharednotes/sharednotes.component';
import { AuthGuard } from './guards/auth.guard';
import { MynotesComponent } from './notes/mynotes/mynotes.component';
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: AppComponent, canActivate: [AuthGuard]},
  { path: 'login', component: LoginComponent, canActivate: [AuthGuard]},
  { path: 'register', component: RegisterComponent, canActivate: [AuthGuard]},
  { path: 'updateuser', component: RegisterComponent, canActivate: [AuthGuard] },
  { path: 'mynotes', component: MynotesComponent, canActivate: [AuthGuard] },
  { path: 'sharednotes', component: SharednotesComponent, canActivate: [AuthGuard] },
  { path: 'createnote', component: NoteComponent, canActivate: [AuthGuard] },
  { path: 'updatenote', component: NoteComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
