import { Router } from '@angular/router';
import { AuthenticationService } from './../auth/authentication.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  updateUser() {
    const user = this.auth.user;
    this.auth.updateUserData(user);
  }

  deleteUser() {
    const user = this.auth.user;
    this.auth.deleteUser(user);
  }

  logOut() {
    this.auth.logout()
  }

  constructor(private auth: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }

}
