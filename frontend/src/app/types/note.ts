export default class Note {
  id: number;
  image_url?: string;
  text: string;
  userID?: string;
  sharable: boolean;
  constructor() {
    this.sharable = false;
    this.text = '';
  }
}
