import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NotesService } from '../notes.service';
import Note from 'src/app/types/note';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {
  note: Note = new Note();
  button = 'add note';
  image: File;

  async onSubmit() {
    if (this.image) {
      await this.noteService.uploadImage(this.image).then((res: string) => {
        this.note.image_url = res;
      });
    }
    if (this.router.url === '/updatenote') {
      await this.noteService.updateNote(this.note);
    } else {
      await this.noteService.createNote(this.note);
    }
    this.router.navigate(['/mynotes']);
  }

  checkURL() {
    if (this.router.url === '/updatenote') {
      this.button = 'update note';
      this.note = this.noteService.note;
    }
  }

  constructor(private noteService: NotesService, private router: Router) {
    this.checkURL();
  }

  ngOnInit() {
  }


}
