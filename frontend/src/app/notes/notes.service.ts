import { BehaviorSubject } from 'rxjs';
import { AuthenticationService } from './../auth/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Note from '../types/note';
import { api_base } from '../constants';
import { Promise, resolve } from 'q';
interface Response {
  success: string;
  results?: Note[];
  result?: string;
  message?: string;
}

@Injectable({
  providedIn: 'root'
})
export class NotesService {
  options: {
    headers?: HttpHeaders,
  } = {};
  private _note: BehaviorSubject<Note>;

  get note(): Note | undefined {
    if (this._note) {
      return this._note.getValue();
    } else {
      return undefined;
    }
  }

  set note(note: Note) {
    if (this._note) {
      this._note.next(note);
    } else {
      this._note = new BehaviorSubject(note);
    }
  }

  getNotes(sharable: boolean) {
    return Promise((resolv, reject) => {
      if (sharable) {
        this.http.get(`${api_base}/sharednotes`, this.options).subscribe((res: Response) => {
          console.log(res);
          if (res.results) { resolv(res.results); }
        });
      } else {
        this.http.get(`${api_base}/mynotes`, this.options).subscribe((res: Response) => {
          console.log(res);
          if (res.results) { resolv(res.results); }
        });
      }
    });
  }

  createNote(note: Note) {
    this.http.post(`${api_base}/note`, note, this.options).subscribe();
  }

  updateNote(note: Note, share?: boolean, shareVal?: boolean) {
    if (share) {
      note.sharable = shareVal;
    }
    this.http.patch(`${api_base}/note`, { note }, this.options).subscribe((res: Response) => {
      console.log(res);
    });
  }

  deleteNote(note: Note) {
    console.log(this.options);

    this.http.delete(`${api_base}/note/${note.id}/${note.userID}`, this.options).subscribe((res: Response) => {
      console.log(res);
    });
  }

  uploadImage(file: File) {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    return Promise((reslov, reject) => {
      this.http.post(`${api_base}/upload`, formData, this.options).subscribe((res: Response) => {
        reslov(res.result);
      });
    });
  }

  constructor(private http: HttpClient, private auth: AuthenticationService) {
    this.options.headers = new HttpHeaders({
      Authorization: this.auth.token
    });
  }

}
