import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NotesService } from '../notes.service';
import Note from 'src/app/types/note';

@Component({
  selector: 'app-mynotes',
  templateUrl: './mynotes.component.html',
  styleUrls: ['./mynotes.component.css']
})

export class MynotesComponent implements OnInit {
  notes: Note[];
  noteHolder: Note;

  updateNote(note: Note, share?: boolean, shareVal?: boolean) {
    this.noteService.updateNote(note, share, shareVal);
  }

  deleteNote(note: Note) {
    this.noteService.deleteNote(note);
    this.router.navigate(['/updatenote']);
  }

  update(note: Note) {
    this.noteService.note = note;
    this.router.navigate(['/updatenote']);
  }

  constructor(private noteService: NotesService, private router: Router) { }

  ngOnInit() {
    this.noteService.getNotes(false).then((res: []) => {
      this.notes = res;
    });
  }

}
