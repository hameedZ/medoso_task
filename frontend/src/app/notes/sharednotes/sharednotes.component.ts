import Note from 'src/app/types/note';
import { Component, OnInit } from '@angular/core';
import { NotesService } from '../notes.service';

@Component({
  selector: 'app-sharednotes',
  templateUrl: './sharednotes.component.html',
  styleUrls: ['./sharednotes.component.css']
})

export class SharednotesComponent implements OnInit {
  notes: Note[];

  constructor(private noteService: NotesService) { }

  ngOnInit() {
    this.noteService.getNotes(true).then((res: []) => {
      this.notes = res;
    });
  }

}
