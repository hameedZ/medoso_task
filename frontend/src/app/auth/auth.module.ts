import { MustMatchDirective } from './must-match.directive';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-routing.module';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from '../auth/login/login.component';
import { AuthenticationService } from './authentication.service';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    MustMatchDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    AppRoutingModule
  ],
  exports: [
    LoginComponent,
    RegisterComponent
  ],
  providers: [AuthenticationService]
})
export class AuthModule { }
