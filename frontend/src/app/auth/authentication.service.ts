import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import User from './../types/user';
import { Router } from '@angular/router';
import { api_base } from '../constants';

interface Response {
  success: string;
  token?: string;
  result?: User;
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  options: {
    headers?: HttpHeaders
  } = {};
  private _token: BehaviorSubject<string>;
  private _user: BehaviorSubject<User>;
  constructor(private http: HttpClient, private router: Router) {
    this._token = new BehaviorSubject(localStorage.getItem('token'));
    this.options.headers = new HttpHeaders({
      Authorization: this.token
    });
  }

  set token(token: string) {
    if (this._token) {
      this._token.next(token);
    } else {
      this._token = new BehaviorSubject(token);
    }
    localStorage.setItem('token', token);
  }

  get token(): string | undefined {
    if (this._token) {
      return this._token.getValue();
    } else {
      return undefined;
    }
  }

  get user(): User | undefined {
    if (this._user) {
      return this._user.getValue();
    } else {
      return undefined;
    }
  }

  set user(user: User) {
    if (this._user) {
      this._user.next(user);
    } else {
      this._user = new BehaviorSubject(user);
    }
  }

  login(user: User) {
    this.http.post(`${api_base}/login`, user).subscribe((res: Response) => {
      this.token = res.token;
      this.router.navigate(['/mynotes']).then(resp => console.log(resp));
    }, e => console.log(e));
  }

  register(user: User) {
    this.http.post(`${api_base}/register`, user).subscribe((res: Response) => {
      this.token = res.token;
      this.router.navigate(['/mynotes']);
    });
  }

  getUserData() {
    return new Promise((resolv, reject) => {
      this.http.get(`${api_base}/user`, this.options).subscribe((res: Response) => {
        if (res.result) { resolv(res.result); }
      });
    });
  }

  updateUserData(user: User) {
    this.http.patch(`${api_base}/user`, user, this.options).subscribe((res: Response) => {
      this.user = res.result;
      this.router.navigate(['/mynotes']);

    });
  }


  deleteUser(user: User) {
    this.http.delete(`${api_base}/user/${user.id}`, this.options).subscribe();
  }

  logout() {
    this.token = '';
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

}
