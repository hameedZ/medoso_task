const Sequelize = require('sequelize')
const db = require('../config/db')

var Note = db.define('note', {
  text: {
    type: Sequelize.STRING,
    field: 'text' // Will result in an attribute that is firstName when user facing but first_name in the database
  },
  imageURL: {
    type: Sequelize.STRING,
    field: 'image_url' // Will result in an attribute that is firstName when user facing but first_name in the database
  },
  userID: {
    type: Sequelize.INTEGER,
    field: 'user_id'
  },
  sharable: {
    type: Sequelize.BOOLEAN,
    field: 'sharable' // Will result in an attribute that is firstName when user facing but first_name in the database
  }
});
Note.sync().then(() => console.log('Notes Table'))

module.exports = Note