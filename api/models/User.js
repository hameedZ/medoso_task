const Sequelize = require('sequelize')
const db = require('../config/db')

const User = db.define('user', {
  fullName: {
    type: Sequelize.STRING,
    field: 'full_name' // Will result in an attribute that is firstName when user facing but first_name in the database
  },
  userName: {
    type: Sequelize.STRING,
    unique: true,
    field: 'user_name' // Will result in an attribute that is firstName when user facing but first_name in the database
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
    field: 'email' // Will result in an attribute that is firstName when user facing but first_name in the database
  },
  password: {
    type: Sequelize.STRING,
    field: 'password'
  }
});

User.sync().then(() => console.log('Users Table'))

module.exports = User
