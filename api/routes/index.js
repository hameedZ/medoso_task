const express = require('express');
const router = express.Router();
const User = require('../models/User'); //User Model
const bcrypt = require('bcrypt'); // a encryption library to hash password
const jwt = require('jsonwebtoken'); //JSON WEB TOKEN library
const Note = require('../models/Note') //Note Model

// register user

router.post('/register1', (req, res, next) => {
  const email = req.body.email
  User.findOne({ where: { email: email } }).then((user) => {
    if (user) {
      res.status(400).json({ success: false, message: 'user already exists' })
    } else {
      // getting hashed password 
      const password = bcrypt.hashSync(req.body.password, 10)
      User.create({
        userName: req.body.username,
        email: req.body.email,
        password: password,
        fullName: req.body.fullname
      }).then(user => {
        var token = jwt.sign(user.dataValues, 'SECRET');
        res.status(201).json({ success: true, token: token })
      })
    }
  }).catch((e) => {
    res.status(403).json({ success: false, message: 'something went wrong' })
  })
});

// login user
router.post('/login', (req, res, next) => {
  const email = req.body.email;
  User.findOne({ where: { email: email } }).then(async user => {
    // there is a user then the email belongs to the user
    if (user) {
      user = user.dataValues
      //  comparing password with the hash stored in DB
      const match = await bcrypt.compare(req.body.password, user.password);
      // if match the passwords givin matches the hashed password stored in the DB
      if (match) {
        // getting a new token to authenticate user
        const token = jwt.sign(user, 'SECRET');
        res.json({ success: true, token: token })
      } 
      // email is valid put wrong password
      else {
        res.status(422).json({ success: false, message: 'password mismatch' })
      }
    } 
    // giving email don't match any user in the DB
    else {
      res.status(422).json({ success: false, message: 'email doesn\'t exists' })
    }
  }).catch(e => {
    res.status(403).json({ success: false, message: 'something went wrong' })
  })
})

// update user data
router.patch('/user', (req, res, next) => {
  try {
    // we recive the JWT token in the headers of the request 
    // the we verify it 
    // if wrong JWT the jwt will throw exeption hence we handle not authorized response inside the catch
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    User.update(req.body.user, { where: { id: decoded.id } }).then(user => {
      console.log(user);
      res.json({ success: true, message: 'updated succesfully' })
    }).catch(e => {
      console.log(e)
      res.status(403).json({ success: false, message: 'something went wrong' })
    });
  } catch (error) {
    console.log(error);
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})

// delete a user
router.delete('/user/:password', (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    const match = bcrypt.match(req.params.password, decoded.password)
    if (match) {
      User.destroy({ where: { id: decoded.id } })
        .then(note => {
          console.log(note)
          res.status(204).json({ success: true, message: 'deleted succesfully' })
        })
        .catch(e => {
          console.log(e)
          res.status(402).json({ success: false, message: 'something went wrong' })
        })
    } else {
      res.status(403).json({ success: false, message: 'user have no permession to delete the note' })
    }
  } catch (error) {
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})

// changing user password --in progress
router.patch('/editpass', (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    const match = bcrypt.match(req.body.oldpass, decoded.password)
    if (match) {
      User.update({ password: decoded.password }, { where: { id: decoded.id } }).then(user => {
        console.log(user);
        res.json({ success: true, message: 'updated succesfully' })
      }).catch(e => {
        console.log(e)
        res.status(404).json({ success: false, message: 'something went wrong' })
      });
    } else {
      res.status(403).json({ success: false, message: 'user have no permession to edit the user data' })
    }
  } catch (error) {
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})

// create a note
router.post('/note', (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    let note = {
      text: req.body.text,
      userID: decoded.id,
      sharable: false
    }
    if (req.body.image_url) {
      note.imageURL = req.body.image_url
    }
    Note.create(note).then(note => {
      if (!note) res.status(404).json({ success: false, message: 'something went wrong' })
      else res.status(201).json({ success: true, results: note })
    }).catch((e) => {
      res.status(400).json({ success: false, message: 'something went wrong' })
    })
  } catch (error) {
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})

// update - shere a note
router.patch('/note', (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    const note = req.body.note
    if (decoded.id == note.userID) {
      Note.update(note, { where: { id: note.id } }).then(note => {
        console.log(note);
        res.json({ success: true, message: 'updated succesfully' })
      }).catch(e => {
        console.log(e)
        res.status(404).json({ success: false, message: 'something went wrong' })
      });
    } else {
      res.status(403).json({ success: false, message: 'user have no permession to edit the note' })
    }
  } catch (error) {
    console.log(error);
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})

// update - shere a note
router.patch('/note2', (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    const note = req.body.note
    if (decoded.id == note.userID) {
      Note.update(note, { where: { id: note.id } }).then(note => {
        console.log(note);
        res.json({ success: true, message: 'updated succesfully' })
      }).catch(e => {
        console.log(e)
        res.status(404).json({ success: false, message: 'something went wrong' })
      });
    } else {
      res.status(403).json({ success: false, message: 'user have no permession to edit the note' })
    }
  } catch (error) {
    console.log(error);
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})

// update - shere a note
router.patch('/note3', (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    const note = req.body.note
    if (decoded.id == note.userID) {
      Note.update(note, { where: { id: note.id } }).then(note => {
        console.log(note);
        res.json({ success: true, message: 'updated succesfully' })
      }).catch(e => {
        console.log(e)
        res.status(404).json({ success: false, message: 'something went wrong' })
      });
    } else {
      res.status(403).json({ success: false, message: 'user have no permession to edit the note' })
    }
  } catch (error) {
    console.log(error);
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})


// update - shere a note
router.patch('/note4', (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    const note = req.body.note
    if (decoded.id == note.userID) {
      Note.update(note, { where: { id: note.id } }).then(note => {
        console.log(note);
        res.json({ success: true, message: 'updated succesfully' })
      }).catch(e => {
        console.log(e)
        res.status(404).json({ success: false, message: 'something went wrong' })
      });
    } else {
      res.status(403).json({ success: false, message: 'user have no permession to edit the note' })
    }
  } catch (error) {
    console.log(error);
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})


// update - shere a note
router.patch('/note5', (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    const note = req.body.note
    if (decoded.id == note.userID) {
      Note.update(note, { where: { id: note.id } }).then(note => {
        console.log(note);
        res.json({ success: true, message: 'updated succesfully' })
      }).catch(e => {
        console.log(e)
        res.status(404).json({ success: false, message: 'something went wrong' })
      });
    } else {
      res.status(403).json({ success: false, message: 'user have no permession to edit the note' })
    }
  } catch (error) {
    console.log(error);
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})
// update - shere a note
router.patch('/note6', (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    const note = req.body.note
    if (decoded.id == note.userID) {
      Note.update(note, { where: { id: note.id } }).then(note => {
        console.log(note);
        res.json({ success: true, message: 'updated succesfully' })
      }).catch(e => {
        console.log(e)
        res.status(404).json({ success: false, message: 'something went wrong' })
      });
    } else {
      res.status(403).json({ success: false, message: 'user have no permession to edit the note' })
    }
  } catch (error) {
    console.log(error);
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})
// update - shere a note
router.patch('/note7', (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    const note = req.body.note
    if (decoded.id == note.userID) {
      Note.update(note, { where: { id: note.id } }).then(note => {
        console.log(note);
        res.json({ success: true, message: 'updated succesfully' })
      }).catch(e => {
        console.log(e)
        res.status(404).json({ success: false, message: 'something went wrong' })
      });
    } else {
      res.status(403).json({ success: false, message: 'user have no permession to edit the note' })
    }
  } catch (error) {
    console.log(error);
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})
// update - shere a note
router.patch('/note8', (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    const note = req.body.note
    if (decoded.id == note.userID) {
      Note.update(note, { where: { id: note.id } }).then(note => {
        console.log(note);
        res.json({ success: true, message: 'updated succesfully' })
      }).catch(e => {
        console.log(e)
        res.status(404).json({ success: false, message: 'something went wrong' })
      });
    } else {
      res.status(403).json({ success: false, message: 'user have no permession to edit the note' })
    }
  } catch (error) {
    console.log(error);
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})
// deleting note
router.delete('/note/:id/:userID', (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    console.log(decoded.id, req.params.userID, req.params.id);

    if (decoded.id == req.params.userID) {
      Note.destroy({ where: { id: req.params.id } })
        .then(note => {
          console.log(note)
          res.status(204).json({ success: true, message: 'deleted succesfully' })
        })
        .catch(e => {
          console.log(e)
          res.status(404).json({ success: false, message: 'something went wrong' })
        })
    } else {
      res.status(403).json({ success: false, message: 'user have no permession to delete the note' })
    }
  } catch (error) {
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})

// retrive user notes
router.get('/mynotes', (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    Note.findAll({ where: { userID: decoded.id } }).then(notes => {
      if (!notes) res.json({ success: true, results: [] })
      else res.json({ success: true, results: notes })
    }).catch((e) => {
      res.status(404).json({ success: false, message: 'something went wrong' })
    })
  } catch (error) {
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})

// retrive shared notes
router.get('/sharednotes', (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers['authorization'], 'SECRET')
    if (decoded) {
      Note.findAll({ where: { sharable: true } }).then(notes => {
        if (!notes) res.json({ success: success, results: [] })
        else res.json({ success: true, results: notes })
      }).catch((e) => {
        res.status(404).json({ success: false, message: 'something went wrong' })
      })
    }
  } catch (error) {
    res.status(401).json({ success: false, message: 'Not Authorized' })
  }
})

router.post('/upload', function (req, res) {
  let sampleFile, uploadPath;

  if (Object.keys(req.files).length == 0) {
    res.status(400).json({success: false, message: 'No files were uploaded.'});
    return;
  }
  console.log('req.files >>>', req.files); // eslint-disable-line
  sampleFile = req.files.sampleFile;

  uploadPath = __dirname + '/uploads/' + sampleFile.name;

  sampleFile.mv(uploadPath, function (err) {
    if (err) {
      return res.status(500).send(err);
    }
    res.status(201).json({success: true, result: uploadPath});
  });
});

module.exports = router;
